import React, { Component } from 'react';

import Home from './components/Home';
import Search from './components/Search';
import Detail from './components/Detail';

import {
  BrowserRouter as Router,
  Route,
  NavLink
} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="wap-container">
          
          <div className="navbar">
            <div className="container">
              <ul className="nav navbar-nav">
                <li><NavLink  exact={true} activeClassName="active" to="/">Home</NavLink></li>
                <li><NavLink activeClassName="active" to="/search">Search</NavLink></li>
              </ul>
            </div>
          </div>

          <div className="container">
            <Route exact path="/" component={Home} />
            <Route path="/search" component={Search} />
            <Route path="/make/model/:id" component={Detail} />
          </div>

        </div>
      </Router>
    );
  }
}

export default App;

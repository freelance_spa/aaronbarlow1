import React, { Component } from 'react';
import { fetchHomeData } from '../actions/catalog.js';

import { connect } from 'react-redux';

class Home extends Component {

	componentDidMount() {
		this.props.fetchHomeData();
	}

	render() {
		const { carOfTheWeek } = this.props;
		return (
			<div>
				{carOfTheWeek.modelId !== undefined && carOfTheWeek.review
					? (<div>
						<div>modelId:  { carOfTheWeek.modelId } </div>
						<div>Review: { carOfTheWeek.review }</div>
					</div>)
					: (<div>Loadding</div>)}
			</div>
		);
	}
}

const mapStateToProps = ({ carOfTheWeek }) => ({
	carOfTheWeek
})

export default connect(mapStateToProps, { fetchHomeData })(Home);
import React, { Component } from 'react';
import { fetchModelData } from '../actions/catalog.js';

import { connect } from 'react-redux';

class Detail extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: true
		};
	}
	

	componentWillMount() {
		const { match } = this.props;
		const { params } = match;
		this.props.fetchModelData(params.id);
		this._handleImageLoaded = this._handleImageLoaded.bind(this);
		this._handleImageErrored = this._handleImageErrored.bind(this);
	}

	_handleImageLoaded() {
		this.setState({
			loading: false
		});
	}

	_handleImageErrored() {

	}

	render() {
		console.log(this.props);
		const { model } = this.props;
		return (
			<div>
				{model.id !== undefined
					? (<div className={ this.state.loading ? 'hidden' : ''}>
						<span>ID: {model.id}</span>
						<div>makeId: {model.makeId}</div>
						<div>name: {model.name}</div>
						<div>Price: {model.price}</div>
						<img 
							onLoad={this._handleImageLoaded}
          		onError={this._handleImageErrored}
							src={model.imageUrl} 
							alt="a" 
							className="img-responsive" />
					</div>)
					: (<div>Loading data</div>)
				}
				<div className={ (!this.state.loading && model.id !== undefined) ? 'hidden' : ''}>Loading data</div>
			</div>
		);
	}
}

const mapStateToProps = ({ model }) => ({
	model
})

export default connect(mapStateToProps, { fetchModelData })(Detail);
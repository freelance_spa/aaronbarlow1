import React, { Component } from 'react';

import { fetchMakesData, fetchModelsData } from '../actions/catalog.js';

import { connect } from 'react-redux';

class Search extends Component {

	constructor(props) {
		super(props);
		this.state = {
			disabled: true
		};
		this._onChangeMake = this._onChangeMake.bind(this);
		this._onChangeModel = this._onChangeModel.bind(this);
		this._onClick = this._onClick.bind(this);
	}

	componentDidMount() {
		this.props.fetchMakesData();
	}

	_onChangeMake(e) {
		const value = e.target.value;
		if (value !== "") {
			this.props.fetchModelsData(value);
		} else {
			this.setState({
				disabled: true
			});
		}
	}

	_onChangeModel(e) {
		const value = e.target.value;
		if (value !== "") {
			this.setState({
				disabled: false
			});
		} else {
			this.setState({
				disabled: true
			});
		}
	}

	_onClick(e) {
		var id = this.selectModel.value;
		let { history } = this.props;
		history.push('/make/model/' + id);
	}

	render() {

		const { makes, models } = this.props;

		return (
			<div className="row">

				<div className="col-xs-4">
					<div className="form-group">
						<label className="col-sm-4 control-label">Car makes: </label>
						<div className="col-sm-8">
							<select onChange={this._onChangeMake} className="form-control" required="required">
								<option value="">Select makes</option>
								{makes.map(make => (<option value={make.id} key={make.id}>{make.name}</option>))}
							</select>
						</div>
					</div>
				</div>

				<div className="col-xs-4">
					<div className="form-group">
						<label className="col-sm-4 control-label">Models: </label>
						<div className="col-sm-8">
							<select onChange={this._onChangeModel} ref={(input) => { this.selectModel = input; }} className="form-control" required="required">
								<option value="">Select model</option>
								{models.map(make => (<option value={make.id} key={make.id}>{make.name}</option>))}
							</select>
						</div>
					</div>
				</div>

				<div className="col-xs-2">
					<button onClick={this._onClick} type="submit" className="btn btn-primary" disabled={this.state.disabled}>Submit</button>
				</div>

			</div>
		);
	}
}

const mapStateToProps = ({ makes, models }) => ({
	makes,
	models
});

export default connect(mapStateToProps, { fetchMakesData, fetchModelsData })(Search);

import React from 'react';
import Home from './Home';
import renderer from 'react-test-renderer';

import reducers from '../reducers';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import thunk from 'redux-thunk';

let store = createStore(
	reducers,
	applyMiddleware(thunk)
);

test('Link changes the class when hovered', () => {

	const component = renderer.create(
		<Provider store={store}>
			<Home />
		</Provider>
	);

	let tree = component.toJSON();
	expect(tree).toMatchSnapshot();

});
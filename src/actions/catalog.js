import axios from 'axios';

// Fetch data for homepage
function fetchHomeDataSuccess(payload) {
    return {
        type: "FETCH_HOME_SUCCESS",
        payload
    }
}

export const fetchHomeData = () => {

    const URL_API = window.URL_API + "/";

    return (dispatch) => {
        axios.get(URL_API)
            .then(function (response) {
                dispatch(fetchHomeDataSuccess(response.data));
            })
            .catch(function (error) {
                // console.log(error);
            });
    }
}

// Fetch data for search page - makes
function fetchMakesDataSuccess(payload) {
    return {
        type: "FETCH_MAKES_SUCCESS",
        payload
    }
}

export const fetchMakesData = () => {

    const URL_API = window.URL_API + "/makes";

    return (dispatch) => {
        axios.get(URL_API)
            .then(function (response) {
                dispatch(fetchMakesDataSuccess(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

// Fetch data for models

function fetchModelsDataSuccess(payload) {
    return {
        type: "FETCH_MODELS_SUCCESS",
        payload
    }
}

export const fetchModelsData = (id = '') => {

    const URL_API = window.URL_API + "/models?id=" + id;

    return (dispatch) => {
        axios.get(URL_API)
            .then(function (response) {
                dispatch(fetchModelsDataSuccess(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

// Fetch detail model
function fetchModelDataSuccess(payload) {
    return {
        type: "FETCH_MODEL_SUCCESS",
        payload
    }
}

export const fetchModelData = (id = '') => {

    const URL_API = window.URL_API + "/model?id=" + id;

    return (dispatch) => {
        axios.get(URL_API)
            .then(function (response) {
                dispatch(fetchModelDataSuccess(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}
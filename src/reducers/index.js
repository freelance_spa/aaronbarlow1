
import carOfTheWeek from './carOfTheWeek';
import makes from './makes';
import models from './models';
import model from './model';

import { combineReducers } from 'redux';

const freelanceApp = combineReducers({
  carOfTheWeek,
  makes,
  models,
  model
});

export default freelanceApp;
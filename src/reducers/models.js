const models = (state = [], action) => {
	const { payload } = action;
	switch (action.type) {
		case "FETCH_MODELS_SUCCESS":
			return payload;
		default:
			return state;
	}
};

export default models;
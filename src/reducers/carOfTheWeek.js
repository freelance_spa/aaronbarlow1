const carOfTheWeek = (state = {}, action) => {
	const { payload } = action;
	switch (action.type) {
		case "FETCH_HOME_SUCCESS":
			return payload;
		default:
			return state;
	}
};

export default carOfTheWeek;
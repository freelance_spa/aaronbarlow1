const makes = (state = [], action) => {
	const { payload } = action;
	switch (action.type) {
		case "FETCH_MAKES_SUCCESS":
			return payload;
		default:
			return state;
	}
};

export default makes;
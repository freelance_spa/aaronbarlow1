
import makes from './makes';

describe('Request Reducer', () => {

	const makesData = '[ { "id": 10, "name": "Porsche" }, { "id": 20, "name": "Nissan" }, { "id": 30, "name": "BMW" }, { "id": 40, "name": "Audi" }, { "id": 50, "name": "Mazda" } ]';

	it('has a default state', () => {
		expect(makes(undefined, { type: '' }), ).toEqual([]);
	});

	it('fetch data success', () => {
		expect(makes(undefined, { type: 'FETCH_MAKES_SUCCESS', payload: JSON.parse(makesData) })).toEqual(
			JSON.parse(makesData)
		);
	});

});
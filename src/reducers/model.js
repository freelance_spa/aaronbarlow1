const model = (state = {}, action) => {
	const { payload } = action;
	switch (action.type) {
		case "FETCH_MODEL_SUCCESS":
			return payload;
		default:
			return state;
	}
};

export default model;
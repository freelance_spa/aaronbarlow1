import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

let store = createStore(
	reducers,
	applyMiddleware(thunk)
);

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root'));
registerServiceWorker();

console.log("init", store.getState());

store.subscribe(() =>
  console.log("success fetch", store.getState())
);
